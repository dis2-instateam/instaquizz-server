var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var questionList = [];
var id = 0;
var lastQuestion;

// Possible states for questions:
// 1. created: inserted in the stack of questions, but not used yet
// 2. active: currently distributed to the audience clients to be answered
// 3. asked: question has already been asked and answered

// We only need to parse JSON Objects
app.use(bodyParser.json());
app.use(express.static('result'));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.get('/', function (req, res) {
	res.send('It works! This is the root of the InstaQuizz Server; nothing to do here!\n');
});

app.get('/api/audience/getQuestion', function(req, res) {
	if(!activeQuestion()) {
		res.status(500).send('Currently no question!\n');
	} else {
		res.status(200).send(activeQuestion());
	}
});

app.post('/api/audience/postAnswer', function(req, res) {
	// If theres no question, this post is pointless
	if(!activeQuestion()) {
		res.status(500).send('No question was asked!\n');
		return;
	} 

	// If thers no answer provided in the post, it cannot be processed
	var answer = req.body.answer;
	var id = req.body.id;

	// Check for valid json
	if(!id) {
		res.status(500).send('Invalid JSON in body!\n');
		return;
	}

	// Check if the answer is intended for the active question
	if(activeQuestion().id !== id) {
		res.status(500).send('Answer for question ' + id + ', but active question is ' + activeQuestion().id + '\n');
		return;
	}

	// If everything is fine, add the answer into the array of all answers
	activeQuestion().answers.push(answer);
	res.status(200).send('Answer posted!\n');
});


app.post('/api/lecturer/addQuestion', function(req, res) {
	// Get the quesiton
	var question = req.body;

	// Check if all mandatory fields exist
	if(!question.question || !question.type) {
		res.status(500).send('Could not read question!\n');
		return;
	} 

	// Add an unique id to the question
	question.id = ++id;

	// Add the status to the quesiton
	question.status = 'created';

	// Add the array that is supposed to contain the answers
	question.answers = [];

	// Push the question into the array of all questions
	questionList.push(question);

	// Return 200 and be happy
	res.status(200).send('Question posted\n');
});

app.post('/api/lecturer/editQuestion', function(req, res) {
	var questionId = req.body.id;

	if(!questionId || !req.body.question || !req.body.type) {
		res.status(500).send('One or more of the following not specified: id/question/type!');
		return;
	}

	if(!questionById(questionId)) {
		res.status(500).send('Question with id ' + questionId + ' does not exist!');
		return;
	}

	// Find the question that is going to be edited
	for(var i = 0; i < questionList.length; i++) {
		if(questionList[i].id === questionId) {
			// Replace the question with the edited one
			questionList[i] = req.body;

			// 'Saul goodman, return here
			res.status(200).send();
			return;
		}
	}	
});

app.post('/api/lecturer/removeQuestion', function(req, res) {
	if(!req.body.id) {
		res.status(500).send('No question id specified');
		return;
	}

	// Find the question to be removed
	for(var i = 0; i < questionList.length; i++) {
		if(questionList[i].id === req.body.id) {
			// Remove the element
			questionList.splice(i, 1);

			// Nothing more to do - return
			res.status(200).send();
			return;
		}
	}

		// If we get here, the question to be removed does not exist
		res.status(500).send('Question with id ' + req.body.id + ' does not exist!');
	});	

app.get('/api/lecturer/getAllQuestions', function(req, res) {
	res.status(200).send(questionList);
});

app.get('/api/lecturer/getLastResult', function(req, res) {
	if(!lastQuestion) {
		res.status(500).send('No answered question yet!');
		return;
	}

	res.status(200).send(lastQuestion);
});

app.post('/api/lecturer/askQuestion', function(req, res) {
	var id = req.body.id;

	var question = questionById(id);

	// Check for valid question for the given id
	if(!question) {
		res.status(500).send('Could not find question with id ' + id + '\n');
		return;
	}

	if(activeQuestion()) {
		res.status(500).send('There is still another question active (with id ' + activeQuestion().id + ')!\n');
		return;
	}

	if(question.status === 'asked') {
		res.status(500).send('Question has already been asked. Asking old questions again is not implemented yet!\n');
		return;
	}

	question.status = 'active';
	res.status(200).send('Question ' + question.id + ' is now active!\n');
});

app.get('/api/lecturer/stopAskingQuestion', function(req, res) {
	var question = activeQuestion();

	// Check if there's actually  question active
	if(!question) {
		res.status(500).send('No question active!\n');
		return;
	}

	// Change the status to 'asked', question time is over!
	question.status = 'asked';
	lastQuestion = question;

	// Send back the asked question (including the answers) for convenience reasons
	res.status(200).send(question);
});

var activeQuestion = function() {
	for(var i = 0; i < questionList.length; i++) {
		if(questionList[i].status === 'active') {
			return questionList[i];
		}
	}
};

var questionById = function(id) {
	for(var i = 0; i < questionList.length; i++) {
		if(questionList[i].id === id) {
			return questionList[i];
		}
	}
};

var server = app.listen(3000, function () {

	var host = server.address().address;
	var port = server.address().port;

	console.log('InstaQuizz server listening at http://%s:%s', host, port);
});