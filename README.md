# InstaQuizz Server
This is the server that handles our InstaQuizz system.

## Installation
* Install npm and nodejs on your system
* run __npm install__

## Start the server
* node server.js


## DigitalOcean commands

`ssh jonasvogel@46.101.187.127`

Restart nginx: `sudo service nginx restart`

Restart nodejs server `pm2 restart server`

Start audience client: `tmux`, then `cd /home/jonasvogel/instaquizz-audience-client/ && ionic serve &`, then `^b :detach`



## API description
### Audience
#### Get the question

* Method: `GET`
* Path: `/api/audience/getQuestion`
* Returns:
```json
{
	"question":"How many peas?",
	"type":"numerical",
	"id":2
}
```

#### Post the answer to the active question

* Method: `POST`
* Path: `api/audience/postAnswer`
* Body: 
```json
{
	"id":2,
	"answer":345
}
```

### Lecturer
#### Add a question to the list of questions, type specific field is optional and has different ccontent depending on questionType (we have to further decide on this)

* Method: `POST`
* Path: `api/lecturer/addQuestion`
* Body: 
```json
{
	"question":"How many peas?",
	"type":"numerical"
}
```

#### Edit a question that is already on the list
* Method: `POST`
* Path: `api/lecturer/editQuestion`
* Body:
```json
{
	"id": 1
}
```

#### Remove a question from the list
* Method `POST`
* Path `api/lecturer/removeQuestion`
* Body:
```json
{
	"id": 1
}
```

#### Get the list of all questions that are stored on the server

* Method: `GET`
* Path: `api/lecturer/getAllQuestions`
* Returns:
```json
[{
	"question":"How many peas?",
	"type":"numerical",
	"status": "asked",
	"id": 2,
	"answers": [34,234,654,234]
}]
```

#### Get the result of the __last__ question that was asked and stopped
* Method: `GET`
* Path: `api/lecturer/getLastResult`
* Returns:
```json
{
	"question":"How many peas?",
	"type":"numerical",
	"id": 2,
	"answers": [34,234,654,234]
}
```

#### Ask a question that is in the list of questions, status is changed to __active__

* Method: `POST`
* Path: `api/lecturer/askQuestion`
* Body: 
```json
{
	"id":2
}
```

#### Stop asking a question

* Method: `GET`
* Path: `api/lecturer/stopAskingQuestion`
* Returns:
```json
{
	"question":"How many peas?",
	"type":"numerical",
	"id": 2,
	"answers": [34,234,654,234]
}
```

### Type specific
#### Possible question types:
* numerical
* yes-no
* likert
* quiz
* mc


#### Numeric
```json
{
		"typeSpecific": 
		{
			"min":42,
			"max":666	
		}
}
```
#### Quiz __AND__ MC
```json
{
		"typeSpecific": 
		{
			"choices":
			[
				"Banana",
				"Apple",
				"Strawberry",
				"Lemon"
			]
		}
}
```


#### Answer Types
* Yes-No: `['Yes','Yes','No']`
* Numerical: `[123,345,345,324,]`
* Likert: `[1,3,3,2,1,4,5]`
* Quiz: `['Banana','Strawberry','Apple']`
* MC: `[['Banana, 'Strawberry'],['Apple'],['Apple','Lemon']]`