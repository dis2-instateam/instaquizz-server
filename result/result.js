'use strict';

function calcArray(choices, answers) {
  var a = [];

  if( !($.isArray(choices)) ){
    choices = Object.keys(choices).map(function (key) {return choices[key]});
  }

  function reduce(n, val) {
    return n + (val === choices[i]);
  }

  //count all occurences
  for ( var i = 0; i < choices.length; i++ ) {
    var count = answers.reduce(reduce, 0);
    a.push([choices[i],count]);
  }

  //delete duplicates
  $.unique(a);
  return a;
}

function mergeAnswers(answers){
  var a = [];
  for (var i = 0; i < answers.length; i++) {
    a = a.concat(answers[i]);
  };
  return a;
}

function splitArray(a){
  var b = [], c = [];
  //split in two arrays to make visualization easier
  for (var i = 0; i < a.length; i++) {
    b.push(a[i][0]);
    c.push(a[i][1]);
  }
  return [b,c];
}

function displayPieChart(arraySummarized, resultObject){
  // Radialize the colors
  Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
      radialGradient: {
        cx: 0.5,
        cy: 0.3,
        r: 0.7
      },
      stops: [
        [0, color],
        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
      ]
    };
  });

  // Build the chart
  $('#container').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ""
    },
    plotOptions: {
      pie: {
        allowPointSelect: false,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          },
          connectorColor: 'silver'
        }
      }
    },
    series: [{
      name: "Total",
      data: [{
        name: arraySummarized[0][1],
        y: arraySummarized[1][1],
        sliced: true,
        selected: false
      },
      {
        name: arraySummarized[0][0], 
        y: arraySummarized[1][0]
      }]
    }]
  });
}

function displayBarChart(arraySummarized, resultObject){
  $('#container').highcharts({
    chart: {
      type: 'bar'
    },
    title: {
      text: ""
    },
    xAxis: {
      categories: arraySummarized[0]
    },
    series: [{
      name: 'Number of Answers',
      data: arraySummarized[1] 
    }]
  });
}

function displayNumericalBarChart(arraySummarized, resultObject){
  var numbers = [];

  for (var i = 0; i < arraySummarized.length; i++) {
    numbers.push( [parseInt(arraySummarized[i][0]), arraySummarized[i][1] ]);
  };

  numbers.sort(function(a, b){return a[0]-b[0]});
  numbers = splitArray(numbers); 

  var min = resultObject.typeSpecific.min;
  var max = resultObject.typeSpecific.max;
  var blockArray1 = [];
  var blockArray2 = [];
  var countOccurences = 0;
  var count = min;
  var i = 0;

  while((numbers[0][i] >= count) && (count <= max)){
    //check if actual number is in the actual range
    if((numbers[0][i] >= count) && (numbers[0][i] < 100+count)){
      countOccurences++; 
      //check if i is still in range of array
      if(i+1 < numbers[0].length){ 
        i++;    
      }
      else{
        blockArray1.push("" + count + "-" + (count+99));
        blockArray2.push(countOccurences);
        //fill up with 0 and break while loop
        while(count < max){
          blockArray1.push("" + count + "-" + (count+99));
          blockArray2.push(0);
          count = count + 100;
        }
        break;
      }
    }
    //save actual range and go to next one
    else{
      blockArray1.push("" + count + "-" + (count+99));
      blockArray2.push(countOccurences);
      countOccurences = 0;
      count = count + 100;
    }
  }

  $('#container').highcharts({
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: blockArray1
    },
    yAxis: {
      title: {
        text: '# People'
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [{
      name: 'Answered Value',
      data: blockArray2
    }]
  });
}


$(document).ready(function() {
  $.get('http://46.101.187.127/server/api/lecturer/getLastResult', {}, function(response) {
    displayResult(response);
  });
});

function displayResult(response){
  var resultObject;

  if(!response) {
    // resultObject = {"question":"How many peas?",
    // "type":"numerical",
    // "typeSpecific": {
    //   "correctAnswer":345 },
    //   "id": 2,
    //   "answers": [34,34,50,61,234,935,654,2,234]
    // }; 
  } else {
    resultObject = response;
  }

  document.getElementById("question").innerHTML = resultObject.question;
  document.getElementById("numberOfAnswers").innerHTML = resultObject.answers.length;

  //count result
  var arraySummarized = [];

  //display charts depending on question type
  switch(resultObject.type) {
    case "numerical":
    arraySummarized = calcArray(resultObject.answers, resultObject.answers);
    displayNumericalBarChart(arraySummarized, resultObject);
    break;

    case "yes-no":
    arraySummarized = calcArray(["Yes","No"], resultObject.answers);
    arraySummarized = splitArray(arraySummarized);
    displayPieChart(arraySummarized, resultObject);
    break;

    case "mc": 
    arraySummarized = mergeAnswers(resultObject.answers);
    arraySummarized = calcArray(resultObject.typeSpecific.choices, arraySummarized);
    arraySummarized = splitArray(arraySummarized);
    displayBarChart(arraySummarized, resultObject);
    break;

    case "quiz":
    arraySummarized = calcArray(resultObject.typeSpecific.choices, resultObject.answers);
    arraySummarized = splitArray(arraySummarized);
    displayBarChart(arraySummarized, resultObject);
    break;

    case "likert":
    arraySummarized = calcArray([1,2,3,4,5], resultObject.answers);
    arraySummarized = splitArray(arraySummarized);
    displayBarChart(arraySummarized, resultObject);
    break;

    default: document.getElementById("question").innerHTML = "ERROR: no valid question type";         
  }
}